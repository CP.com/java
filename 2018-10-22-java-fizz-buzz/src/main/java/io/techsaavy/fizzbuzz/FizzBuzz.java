package io.techsaavy.fizzbuzz;

class FizzBuzz {
    void doFizzBuzzV1() {
        for (int i = 1; i <= 100; i++) {
            boolean fizz = i % 3 == 0;
            boolean buzz = i % 5 == 0;
            if (fizz && buzz) {
                System.out.print("FizzBuzz ");
            } else if (fizz) {
                System.out.print("Fizz ");
            } else if (buzz) {
                System.out.print("Buzz ");
            } else {
                System.out.print(i + " ");
            }
        }
    }

    void doFizzBuzzV2() {
        for (int i = 1; i <= 100; i++) {
            // print 'FizzBuzz' in place of the number for numbers divisible by 3 and 5
            if (i % (3 * 5) == 0) {
                System.out.print("FizzBuzz ");
                // print 'Fizz' in place of the number for numbers divisible by 3
            } else if (i % 3 == 0) {
                System.out.print("Fizz ");
                // print 'Buzz' in place of the number for numbers divisible by 5
            } else if (i % 5 == 0) {
                System.out.print("Buzz ");
            } else {
                System.out.print(i + " ");
            }
        }
    }

    public void doFizzBuzzWrongV1() {

        for (int i = 1; i <= 100; i++) {

            if (i % 3 == 0) {
                System.out.print("Fizz ");
                // print 'Buzz' in place of the number for numbers divisible by 5
            } else if (i % 5 == 0) {
                System.out.print("Buzz ");
            }
            // print 'FizzBuzz' in place of the number for numbers divisible by 3 and 5
            else if (i % (3 * 5) == 0) {
                System.out.print("FizzBuzz ");
                // print 'Fizz' in place of the number for numbers divisible by 3
            } else {
                System.out.print(i + " ");
            }
        }
    }

    public void doFizzV1() {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.print("Fizz ");
            } else {
                System.out.print(i + " ");
            }
        }
    }

    public void doBuzzV1() {
        for (int i = 1; i <= 100; i++) {
            if (i % 5 == 0) {
                System.out.print("Buzz ");
            } else {
                System.out.print(i + " ");
            }
        }
    }

    public void doFizzBuzzStandaloneV1() {
        for (int i = 1; i <= 100; i++) {
            if (i % 15 == 0) {
                System.out.print("FizzBuzz ");
            } else {
                System.out.print(i + " ");
            }
        }
    }
}
